  module.exports = {
    "/resources/*": {
      target: "http://localhost:8083/public",
      source: false
    },
    "/admin-config.json": {
      target: 'http://alo-cms.local:8080/admin-config.json',
      ignorePath: true,
      changeOrigin: true,
      secure: false
  },
  "/api/login": {
      target: 'http://alo-cms.local:8080/api/login',
      ignorePath: true,
      changeOrigin: true,
      secure: false
  },
  "/api/logout": {
      target: 'http://alo-cms.local:8080/api/logout',
      ignorePath: true,
      changeOrigin: true,
      secure: false
  },
  "/auth-config.json": {
      target: 'http://alo-cms.local:8080/auth-config.json',
      ignorePath: true,
      changeOrigin: true,
      secure: false
  },
  "/translations-config.json": {
      target: 'http://alo-cms.local:8080/translations-config.json',
      ignorePath: true,
      changeOrigin: true,
      secure: false
  },
  "/api/languages-config.json": {
      target: 'http://alo-cms.local:8080/api/languages-config.json',
      ignorePath: true,
      changeOrigin: true,
      secure: false
  },
  "/api/languages/get": {
      target: 'http://alo-cms.local:8080/api/languages/get',
      ignorePath: true,
      changeOrigin: true,
      secure: false
  },
  "/api/languages/save": {
      target: 'http://alo-cms.local:8080/api/languages/save',
      ignorePath: true,
      changeOrigin: true,
      secure: false
  },
  "/api/supported-locales/get": {
      target: 'http://alo-cms.local:8080/api/supported-locales/get',
      ignorePath: true,
      changeOrigin: true,
      secure: false
  },
  "/api/languages/toggle-enable": {
      target: 'http://alo-cms.local:8080/api/languages/toggle-enable',
      ignorePath: true,
      changeOrigin: true,
      secure: false
  },
  "/api/languages/delete": {
    target: "http://alo-cms.local:8080/api/languages/delete",
    ignorePath: true,
    changeOrigin: true,
    secure: false
  },
  "*": {
    target: "http://localhost:3001",
    secure: false
  }
}
