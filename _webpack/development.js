const path = require('path')
const webpack = require('webpack')
const baseConfig = require('./_base')
const proxy = require('./development/proxy')

baseConfig.module.loaders.push({
    test: /\.js$/,
    exclude: /(node_modules|bower_components)/,
    loader: 'babel-loader',
    query: {
        plugins: ['transform-runtime', 'transform-decorators-legacy'],
        presets: ["es2015", "stage-0", "react", "react-hmre"]
    }
})

baseConfig.plugins.push(
    new webpack.DefinePlugin({
        DEVELOPMENT: JSON.stringify(true),
        'process.env': {
            'NODE_ENV': JSON.stringify('development')
        }
    })
)

baseConfig.devServer = {    
    historyApiFallback: true,
    host: 'localhost',
    port: 8083,
    proxy: proxy
}

module.exports = baseConfig
