const path = require('path')
const webpack = require('webpack')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

var nodeModulesDir = path.join(__dirname, '/../node_modules')
var commonModuleDir = path.join(__dirname, '/../src/_common')

module.exports = {
  devtool: 'cheap-inline-module-source-map',
  entry: {
    'auth': './src/auth/index.js',
    'admin': './src/admin/index.js',
    'languages': './src/languages/index.js'
  },

  output: {
    path: path.join(__dirname, 'public'),
    filename: '../../public/resources/[name]/[name].bundle.js',
    publicPath: '/'
  },

  module: {
    loaders: [
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract('css')
      }, {
        test: /\.(png|jpg|svg|ttf|eot|woff|woff2)$/,
        loader: 'url?name=[path][name].[ext]&limit=4096'
      }
    ]
  },

  plugins: [
    new ExtractTextPlugin('../../public/resources/[name].css', {allChunks: true}),
    // new webpack.NamedModulesPlugin(),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      filename: '../../public/resources/vendor.js',
      minChunks: 2
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'common',
      filename: '../../public/resources/common.js',
      minChunks: function(module, count) {
        return module.resource && module.resource.indexOf(commonModuleDir) === 0;
      }
    }),
    new webpack.NoErrorsPlugin()
  ],

  resolve: {
    modulesDirectories: ['node_modules'],
    extensions: ['', '.js', '.json', '.css'],
    alias: {
      'common': path.resolve(__dirname, '../src/_common/common'),
      'common-app-config': path.resolve(__dirname, '../src/_common/app-config'),
      'common-jwt': path.resolve(__dirname, '../src/_common/jwt'),
      'admin': path.resolve(__dirname, '../src/admin'),
      'auth': path.resolve(__dirname, '../src/auth'),
      // 'site-options': path.resolve(__dirname, '../src/site-options'),
      // 'translations': path.resolve(__dirname, '../src/translations'),
      'languages': path.resolve(__dirname, '../src/languages')
    }
  },

  resolveLoader: {
    modulesDirectories: ['node_modules'],
    moduleTemplates: ['*-loader', '*'],
    extensions: ['', '.js']
  },

  node: {
    fs: "empty"
  }
}
