const webpack = require('webpack')
const baseConfig = require('./_base')

baseConfig.module.loaders.push({
   test: /\.js$/,
   exclude: /(node_modules|bower_components)/,
   loader: 'babel-loader',
   query: {
      plugins: ['transform-runtime', 'transform-decorators-legacy'],
      presets: ["react", "es2015", "stage-0"]
   }
})

// baseConfig.cache = false

baseConfig.plugins.push(
    new webpack.DefinePlugin({
        DEVELOPMENT: JSON.stringify(false),
        'process.env': {
            'NODE_ENV': JSON.stringify('production')
        }
    }),
    new webpack.optimize.UglifyJsPlugin({
          sourceMap: false,
          minimize: true,
          mangle: false,
          compress: {
             dead_code: true,
             drop_console: true,
             drop_debugger: true,
             unsafe: true,
             warnings: false
          }
     })
)

module.exports = baseConfig
