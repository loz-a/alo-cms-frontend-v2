var path = require('path');
var express = require('express');
var bodyParser = require('body-parser');
var superagent = require('superagent');
var favicon = require('serve-favicon');

var app = express();
app.set('port', 3001);

app.set('token', null);

app.set('views', __dirname + '/templates');
app.set('view engine', 'ejs');

app.use(favicon(__dirname + '/favicon.ico'));
app.use(bodyParser.json());

var moduleMaps = {
    login: 'auth'
};

app.use(function(req, res, next) {
    if (app.get('token')) {
        next()
    }

    superagent
        .post('http://alo-cms.local:8080/api/login')
        .set('Content-Type', 'application/json')
        .send('{"login":"admin","password":"password"}')
        .end(function(err, xhrpResponse) {
            if (err) {
                console.log('Something went wrong during sign-in');
                next(new Error('Something went wrong during sign-in'))
            }
            // console.log(err);
            // console.log(xhrpResponse);
            app.set('token', xhrpResponse.body.token);
        })
});

app.use(function(err, req, res, next) {
    var errorHandler = express.errorHandler();
    errorHandler(err, req, res, next)
});


// app.get('*', function(httpRequest, httpResponse) {
    // var module = httpRequest.params[0].split('/')[1];

app.get('/:module', function(httpRequest, httpResponse) {
    var module = httpRequest.params.module

    if (module in moduleMaps) {
        module = moduleMaps[module];
    }

    superagent
        .get('http://alo-cms.local:8080/api/' + module + '/get-init-state')
        .set('Authorization', 'Bearer ' + app.get('token'))
        .end(function(err, xhrpResponse) {
            var initState = null;

            if (xhrpResponse.ok) {
                initState = xhrpResponse.body.initState;
            }
            // console.dir(xhrpResponse)
            httpResponse.render('index', { module: module, initState: initState })
        });
});

app.get('/', function(req, res) {
    res.send('hello world');
});

app.listen(app.get('port'), 'localhost', function (err) {
    if (err) {
        console.log(err);
        return;
    }

});
