import axios from 'axios'
import globalStore from 'common/stores/global-store'
import jwtDecode from 'jwt-decode'
import api from 'common/api'

export function jwtData(jwtToken) {
  return jwtDecode(jwtToken).data
}

// export function checkHttpStatus(response) {
//   debugger
//     if (response.status >= 200 && response.status < 300) {
//         return response
//     }
//     const error = new Error(response.statusText)
//     error.response = response
//     throw error
// }

export function checkValidateErrors(response) {
  const { validateErrors } = response.data
  if (validateErrors && Object.keys(validateErrors).length) {
    const error = new Error('Validate error')
    error.response = response
    throw error
  }
  return response
}

export function redirect(url) {
  // const token = sessionGet('token')
  // const url = token ? `${route}?token=${token}` : route
  location.replace(url)
}

export function httpClient(jwtAuth = true) {
    axios.interceptors.response.use(
        (response) => response,
        (error) => {
            globalStore.error = error
            return Promise.reject(error)
        }
    )

    const instance = axios.create();

    if (jwtAuth) {
        const jwtToken = api.token.read()
        instance.defaults.headers.common['Authorization'] = `Bearer ${jwtToken}`;
    }
    return instance
}

// export function isPromise(obj) {
//   return !!obj && (typeof obj === 'object' || typeof obj === 'function') && typeof obj.then === 'function';
// }

export function translator(globalStore) {
  let translateFunc
  return (translateId) => {
    if (globalStore.hasConfig() && undefined === translateFunc) {
      translateFunc = globalStore.getConfig().translations().translate
    }
    return translateFunc ? translateFunc(translateId) : translateId
  }
}

export const isElement = (element, type) =>
  element && element.type && element.type === type

export const isElementOneOf = (element, types) => {
  if (Array.isArray(types)) {
      for (let i = 0; i < types.length; i++) {
        if (isElement(element, types[i])) return true
      }
  } else {
    return isElement(element, types)
  }
  return false
}

// inspired by https://atticuswhite.com/blog/render-if-conditionally-render-react-components/
export const ifExpr = (predicate) => (expr, elseExpr = null) =>
  predicate
    ? ( typeof expr === 'function' ? (expr.bind(this))() : expr )
    : ( typeof elseExpr === 'function' ? (elseExpr.bind(this))() : elseExpr)
