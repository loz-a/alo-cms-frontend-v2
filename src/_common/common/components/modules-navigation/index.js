import React, { PropTypes } from 'react'
import { observer, inject } from 'mobx-react';
import jq from 'jquery'
import uiSidebar from 'semantic-ui-sidebar'
import withTranslator from 'common-app-config/decorators/with-translator'
import { ifExpr } from 'common/utils'
import './styles.css'

@withTranslator
@inject('globalStore')
@observer
class ModulesNavigation extends React.Component {

  componentDidMount() {
    jq.fn.sidebar = uiSidebar
    const $sb = jq(this._sidebar)

    $sb.sidebar({
      transition: 'overlay',
      context: this._sidebar.parentNode
    })

    this.$sb = $sb
  }

  toggleVisibility = () => {
    this.$sb.sidebar('toggle')
  }

  render() {
    const { globalStore } = this.props
    const ifLoading = ifExpr(globalStore.loading).bind(this)
    const navigationConfig = globalStore.hasConfig() ? globalStore.getConfig().navigation() : null

    return (
      <div className="pushable">

        {this.renderNavigation(navigationConfig)}

        <div className="pusher ui container">
          {ifLoading(this.renderLoading, this.renderChildren(navigationConfig))}
        </div>
      </div>
    )
  }

  renderLoading = () => (
    <div className="ui active inverted dimmer">
      <div className="ui large text loader"></div>
    </div>
  )

  renderChildren = (navigationConfig) => () => this.props.children(navigationConfig, this.toggleVisibility)

  renderNavigation = (navigationConfig) => {
    const { translate } = this.props
    const modulesNavigation = navigationConfig ? navigationConfig.modules : []

    return (
      <nav className="ui sidebar vertical inverted menu" ref={(c) => this._sidebar = c}>
        {modulesNavigation.map(
          (item, idx) => (
            <a  className="item" href={item.link} key={idx}>
              <i className={`${item.icon} icon`}></i> {translate(item.title)}
            </a>
          )
        )}
      </nav>
    )
  }

  static propTypes = {
    globalStore: PropTypes.object.isRequired,
    translate: PropTypes.func.isRequired,
    children: PropTypes.func
  }

}

export default ModulesNavigation
