import React, { PropTypes } from 'react'
import cx from 'classnames'

const RowLoading = ({
  colSpan,
  text = "Loading...",
  warning = true,
  positive = false,
  negative = false
}) => (
  <tr className={cx({warning, positive, negative})}>
    <td colSpan={colSpan}>{text}</td>
  </tr>
)

RowLoading.propTypes = {
  colSpan: PropTypes.number.isRequired,
  text: PropTypes.string,
  warning: PropTypes.bool,
  positive: PropTypes.bool,
  negative: PropTypes.bool
}

export default RowLoading
