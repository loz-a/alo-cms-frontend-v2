import React, { PropTypes } from 'react'

const Head = ({
  header,
  subheader,
  icon="circle info"
}) => (
  <h2 className="ui header">
    <i className={`${icon} icon`}></i>
    <div className="content">
      {header}
      <div className="sub header">{subheader}</div>
    </div>
  </h2>
)

Head.getIconByRouteName = (navigation, routeName) => {
  const filtered = (navigation.modules || []).filter((item) => item.routeName === routeName)
  return (filtered.length) ? filtered[0].icon : ''
}

Head.propTypes = {
  header: PropTypes.string.isRequired,
  subheader: PropTypes.string,
  icon: PropTypes.string
}

export default Head
