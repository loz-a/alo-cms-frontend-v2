import React, { PropTypes } from 'react'
import withTranslator from 'common-app-config/decorators/with-translator'

const Menu = ({
  navigation,
  translate,
  modulesNavigationVisibilityToggler
}) => {
  const moduleNavigation = navigation ? navigation.module : []

  return (
    <div className="ui top attached menu">
      <div className="right menu">
        <a className="item" onClick={modulesNavigationVisibilityToggler}>
          <i className="sidebar icon"></i> {translate('Main Menu')}
        </a>

        {moduleNavigation.map(
          (item, idx) => (
            <a className="item" href={item.link} key={++idx}>
              <i className={`${item.icon} icon`}></i> {translate(item.title)}
            </a>
          )
        )}

      </div>
    </div>
  )
}

Menu.propTypes = {
  navigation: PropTypes.object.isRequired,
  translate: PropTypes.func.isRequired,
  modulesNavigationVisibilityToggler: PropTypes.func.isRequired
}

export default withTranslator(Menu)
