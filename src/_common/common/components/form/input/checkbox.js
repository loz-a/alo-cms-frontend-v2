import React, { PropTypes } from 'react'
import classNames from 'classnames'

const Checkbox = ({
  defaultChecked = true,
  type = 'slider',
  readOnly = false,
  disabled = false,
  isFitted = false,
  checked,
  label,
  name,
  onChange,
  ...rest
}) => {
  const wrapperClsNames = classNames('ui', {
    [type]: true,
    'read-only': !!readOnly,
    disabled: disabled,
    checked: checked,
    fitted: isFitted
  }, 'checkbox')

  return (
    <div className={wrapperClsNames}>
      <input type="checkbox"
        name={name}
        defaultChecked={defaultChecked}
        onChange={onChange}
        { ...rest } />
      <label>{label}</label>
    </div>
  )
}

Checkbox.propTypes = {
  defaultChecked: PropTypes.bool,
  type: PropTypes.oneOf(['', 'radio', 'slider', 'toggle']),
  readOnly: PropTypes.bool,
  disabled: PropTypes.bool,
  isFitted: PropTypes.bool,
  checked: PropTypes.bool,
  label: PropTypes.string,
  name: PropTypes.string,
  onChange: PropTypes.func
}

export default Checkbox
