import React, { PropTypes } from 'react'
import { Link } from 'react-router'

const Delete = ({
  href,
  value,
  onClick
}) => (
  <Link onClick={onClick} className="item" to={href}>
    <i className="large link trash icon"></i>
    <div className="bottom aligned content">{value}</div>
  </Link>
)

Delete.propTypes = {
  href: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onClick: PropTypes.func
}

export default Delete
