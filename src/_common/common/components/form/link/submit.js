import React, { PropTypes } from 'react'
import { Link } from 'react-router'

const Submit = ({
  href,
  value,
  onClick
}) => (
  <Link onClick={onClick} className="item" to={href}>
    <i className="large link thumbs up icon"></i>
    <div className="bottom aligned content">{value}</div>
  </Link>
)

Submit.propTypes = {
  href: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onClick: PropTypes.func
}

export default Submit
