import React, { PropTypes } from 'react'

const Close = ({
  value = 'Close',
  onClick
}) => (
  <a className="close-btn" onClick={onClick}>
    <i className="cancel icon"></i> {value}
  </a>
)

Close.propTypes = {
  value: PropTypes.string.isRequired,
  onClick: PropTypes.func
}

export default Close
