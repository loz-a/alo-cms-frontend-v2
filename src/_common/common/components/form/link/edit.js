import React, { PropTypes } from 'react'
import { Link } from 'react-router'

const Edit = ({
  href,
  value,
  onClick
}) => (
  <Link onClick={onClick} className="item" to={href}>
    <i className="large link edit icon"></i>
    <div className="bottom aligned content">{value}</div>
  </Link>
)

Edit.propTypes = {
  href: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onClick: PropTypes.func
}

export default Edit
