import React, { PropTypes } from 'react'
import { Link } from 'react-router'

const Cancel = ({
  href,
  value,
  onClick
}) => (
  <Link onClick={onClick} className="item" to={href}>
    <i className="large link cancel icon"></i>
    <div className="bottom aligned content">{value}</div>
  </Link>
)

Cancel.propTypes = {
  href: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onClick: PropTypes.func
}

export default Cancel
