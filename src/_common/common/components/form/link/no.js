import React, { PropTypes } from 'react'
import { Link } from 'react-router'

const No = ({
  value = 'No',
  href,
  onClick
}) => (
  <Link onClick={onClick} className="item" to={href}>
    <i className="large link undo icon"></i>
    <div className="bottom aligned content">{value}</div>
  </Link>
)

No.propTypes = {
  href: PropTypes.string.isRequired,
  value: PropTypes.string,
  onClick: PropTypes.func
}

export default No
