import React, { PropTypes } from 'react'

const Yes = ({
  href='',
  value = 'Yes',
  onClick
}) => (
  <a onClick={onClick} className="item" href={href}>
    <i className="large link red remove icon"></i>
    <div className="bottom aligned content">{value}</div>
  </a>
)

Yes.propTypes = {
  value: PropTypes.string,
  href: PropTypes.string,
  onClick: PropTypes.func.isRequired
}

export default Yes
