import React, { PropTypes } from 'react'

const Submit = ({
  value = 'Ok',
  onClick
}) => (
  <button type="submit" onClick={onClick} className="ui button">
    <i className="thumbs up icon"></i>
    {value}
  </button>
)

Submit.propTypes = {
  value: PropTypes.string.isRequired,
  onClick: PropTypes.func
}

export default Submit
