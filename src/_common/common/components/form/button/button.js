import React, { PropTypes } from 'react'

const Button = ({
  icon,
  value,
  onClick
}) => (
  <button type="button" onClick={onClick} className="ui button">
    {icon && <i className={`${icon} icon`}></i>}
    {value}
  </button>
)

Button.propTypes = {
  value: PropTypes.string.isRequired,
  icon: PropTypes.string,
  onClick: PropTypes.func
}

export default Button
