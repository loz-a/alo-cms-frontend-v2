import React, { PropTypes } from 'react'

const Cancel = ({
  value = 'Cancel',
  onClick
}) => (
  <button type="button" onClick={onClick} className="ui button">
    <i className="cancel icon"></i>
    {value}
  </button>
)

Cancel.propTypes = {
  value: PropTypes.string.isRequired,
  onClick: PropTypes.func
}

export default Cancel
