class Validator {

  constructor() {
    this._errors = {}
  }

  getErrors() {
    return Object.assign({}, this._errors)
  }

  getErrorsAsArray() {
    const errors = []
    for (const key in this._errors) if (this._errors.hasOwnProperty(key)) {
      errors.push(this._errors[key])
    }
    return errors
  }

  validate(data, context = {}) {
    this._errors = {}
    for (const key in data) if (data.hasOwnProperty(key)) {
      if (this[key] && typeof this[key] === 'function') {
        this[key](data[key], context[key] || undefined)
      }
    }
    return !Object.keys(this._errors).length
  }
}

export default Validator
