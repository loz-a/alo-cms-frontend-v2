import { observable, action, extendObservable } from 'mobx'
import { httpClient, jwtData, redirect } from 'common/utils'
import { loginUri } from 'common/config'
import api from 'common/api'
import AppConfig from 'common-app-config/store'

const UNAUTORIZED = 401

export default function() {
  let auth = {}
  let config

  extendObservable(this, {
    error: '',
    loading: false
  })

  this.initToken = (token = null) => {
    if (token) api.token.write(token)
    try {
      auth = { username: jwtData(api.token.read()).login }
    } catch(e) {
      api.token.clear()
      redirect(loginUri)
    }
  }

  this.initConfig = action((url, jwtAuth = true) => {
    this.loading = true
    return api.fetchConfig(url, jwtAuth)
      .then(
        action((response) => {
          this.loading = false
          config = new AppConfig(response.data || {})
        }),
        action((error) => {
          const { response } = error
          const redirectLoginUri = response.data.loginUri ?  response.data.loginUri : (loginUri ? loginUri : null)

          this.loading = false
          this.error = response.data
          if (response.status === UNAUTORIZED && redirectLoginUri) {
            redirect(redirectLoginUri)
          }
        })
      )
    })

    this.enableLoading = action(() => this.loading = true)

    this.disableLoading = action(() => this.loading = false)

    this.isLoading = () => this.loading

    this.getAuth = () => ({ ...auth })

    this.hasAuth = () => 'username' in auth

    this.getConfig = () => config

    this.hasConfig = () => (config instanceof AppConfig)
}
