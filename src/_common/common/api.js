import { token } from 'common-jwt/api'
import { fetchConfig } from 'common-app-config/api'

export default {
  fetchConfig,
  token
}
