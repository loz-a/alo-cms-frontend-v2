
export default function(config = {}) {

  const TRANSLATIONS_KEY = 'translations'
  const NAVIGATION_KEY = 'navigation'

  const translationsData = config[TRANSLATIONS_KEY] ? config[TRANSLATIONS_KEY] : {}
  const navigationData = config[NAVIGATION_KEY] ? config[NAVIGATION_KEY] : {}

  const data = Object.keys(config).reduce((acc, key) => {
    if (key !== TRANSLATIONS_KEY && key !== NAVIGATION_KEY) {
      acc[key] = config[key]
    }
    return acc
  }, {})

  this.config = (...args) => {
    if (args.length) return args[0] in data ? data[args[0]] : null
    return data
  }

  this.navigation = () => navigationData

  function translate(translateId) {
    return translationsData[translateId] ? translationsData[translateId] : translateId
  }

  this.translations = () => {
    return {
      data: translationsData,
      translate: translate.bind(this)
    }
  }
}
