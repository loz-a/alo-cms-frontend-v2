import React from 'react'
import { inject } from 'mobx-react'
import { translator } from 'common/utils'

export default (DecoratedComponent) => {

  const WithTranslator = (props) => {
    const translate = translator(props.globalStore)
    return <DecoratedComponent {...props} translate={translate} />
  }

  WithTranslator.WrappedComponent = DecoratedComponent

  return inject('globalStore')(WithTranslator)
}
