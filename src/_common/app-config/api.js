import { httpClient } from 'common/utils'

export const fetchConfig = (configUrl, jwtAuth = true) => {
  return httpClient(jwtAuth).get(configUrl)
}
