import { STORAGE_PREFIX } from 'common/config'

export const token = {
  read(storage = null) {
    return (storage || sessionStorage).getItem(`${STORAGE_PREFIX}token`)
  },
  write(token, storage = null) {
    (storage || sessionStorage).setItem(`${STORAGE_PREFIX}token`, token)
  },
  clear(storage = null) {
    (storage || sessionStorage).removeItem(`${STORAGE_PREFIX}token`)
  }
}
