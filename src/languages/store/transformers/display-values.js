import { createTransformer } from 'mobx'
import { mapObjectFromMap } from '../utils'

export default createTransformer((store) => {
  const dvs = store.displayValues
  return dvs.keys().reduce((acc, id) => {
    const entity = dvs.get(id)
    acc[id] = {
      ...entity,
      locale: mapObjectFromMap(entity, 'locale', store.locales)
    }
    return acc
  }, {})
})
