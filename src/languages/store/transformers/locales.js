import { createTransformer } from 'mobx'

export default createTransformer((store) => {
  const locales = store.locales
  return locales.keys().reduce((acc, id) => {
    acc[id] = { ...locales.get(id) }
    return acc
  }, {})
})
