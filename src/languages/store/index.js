import { extendObservable, observable, action } from 'mobx'
import { normalize } from 'normalizr'
import initStateSchema from './schema'

import transformLocales from './transformers/locales'
import transformDisplayValues from './transformers/display-values'
import transformLanguages from './transformers/languages'

export default function(INIT_STATE = {}) {
  const initState = normalize(INIT_STATE, initStateSchema).entities

  const store = {
    languages: {},
    locales: {},
    displayValues: {},
    supportedLocales: initState.supportedLocales
  }

  extendObservable(store, {
    languages: observable.map(initState.languages),
    locales: observable.map(initState.locales),
    displayValues: observable.map(initState.displayValues)
  })

  this.languages = {
    byId: (id) => {
      const languages = this.languages.all()
      if (!languages[id]) throw new Error(`Language with id: ${id} is not exists in store`)
      return languages[id]
    },

    all: () => transformLanguages(store),

    save: action((data) => {
      const language = { ...data }

      if (undefined !== data['locale']) {
        const locale = data['locale']
        store.locales.set(locale.id, locale)
        language['locale'] = locale.id
      }

      if (undefined !== data['displayValues']) {
        const dvs = data['displayValues']
        const dvsIds = []
        Object.values(dvs).forEach((item) => {
          store.displayValues.set(item.id, { ...item, locale: item.locale.id})
          dvsIds.push(item.id)
        })
        language['displayValues'] = dvsIds
      }

      store.languages.set(data.id, language)
    }),

    delete: (language) => {
      store.locales.delete(language.locale.id),
      Object.keys(language.displayValues).forEach(action((id) => store.displayValues.delete(id)))
      store.languages.delete(language.id)
    },

    size: () => Object.keys(this.languages.all()).length
  }

  this.supportedLocales = {
    all: () => store.supportedLocales
  }

  this.locales = {
    all: () => transformLocales(store),
    byName: (name) => Object.values(this.locales.all()).find((locale) => locale.value === name),
    size: () => Object.keys(this.locales.all()).length
  }

  this.displayValues = {
    all: () => transformDisplayValues(store),
    byId: (id) => {
      const dvs = this.displayValues.all()
      if (!dvs[id]) throw new Error(`DisplayValue with id: ${id} is not exists in store`)
      return dvs[id]
    }
  }
}
