import { schema } from 'normalizr'

const locale = new schema.Entity('locales')
const displayValue = new schema.Entity('displayValues', { locale })

const language = new schema.Entity('languages', {
    locale,
    displayValues: new schema.Array(displayValue)
  })

const supportedLocale = new schema.Entity('supportedLocales', {}, { idAttribute: 'name' })

export default {
  supportedLocales: new schema.Array(supportedLocale),
  languages: new schema.Array(language)
}
