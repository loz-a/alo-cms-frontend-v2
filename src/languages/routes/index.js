import React from 'react'
import { Router, Route, IndexRoute, Redirect } from 'react-router'
import history from 'common/history'

import PageNotFound from 'common/components/page-not-found'
import App from 'languages/components/app'
import Languages from 'languages/components/languages'

import Add from 'languages/components/languages/item/add'
import Edit from 'languages/components/languages/item/edit'
import Delete from 'languages/components/languages/item/delete'
import AddDisplayValue from 'languages/components/languages/item/edit/display-values/item/add'
import EditDisplayValue from 'languages/components/languages/item/edit/display-values/item/edit'
import DeleteDisplayValue from 'languages/components/languages/item/edit/display-values/item/delete'
import config from 'languages/config.js'

const languagesRoutes = config.routes.languages

export default (
  <Router history={history}>

    <Route path={languagesRoutes.index} component={App}>
      <IndexRoute component={Languages}/>

      <Route component={Languages}>
        <Route path={languagesRoutes.add} component={Add} />
        <Route path={languagesRoutes.delete} component={Delete} />
      </Route>

      <Route path={languagesRoutes.edit.index} component={Edit}>
        <Route path={languagesRoutes.edit.displayValues.add} component={AddDisplayValue} />
        <Route path={languagesRoutes.edit.displayValues.edit} component={EditDisplayValue} />
        <Route path={languagesRoutes.edit.displayValues.delete} component={DeleteDisplayValue} />
      </Route>
    </Route>

    <Route path="*" component={PageNotFound} />
  </Router>
)
