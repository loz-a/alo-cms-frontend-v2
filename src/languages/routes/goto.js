import config from 'languages/config'
import history from 'common/history'

const dvPath = config.routes.languages.edit.displayValues

function url(strings, ...values) {
  let step = ''
  let steps = ''
  let result = []
  let root = config.routes
  const replaceParam = (str, param) => str.replace(/:(.*)$/, param)

  for (let i=0; i<values.length+1; i++) {
      steps = strings[i].split('/')
      for (let j=0; j<steps.length; j++) {
        step = steps[j]
        if (!step) continue
        if (!step in root) throw new Error('Invalid path in routes.url()')
        if ({}.toString.call(root[step]).slice(8, -1) === 'Object') {
          if (root[step].index) result.push(replaceParam(root[step].index, values[i]))
          root = root[step]
        } else {
          result.push(replaceParam(root[step], values[i]))
        }
      }
      if (values[i] === location) result = [location.pathname]
  }

  return result.join('/')
}

export const urls = {
  languages: () => url`languages`,
  languageNew: () => url`languages/add`,
  languageEdit: (languageId) => url`languages/edit/${languageId}`,
  languageDelete: (languageId) => url`languages/delete/${languageId}`,
  displayValueNew: (languageId = null) => url`languages/edit/${languageId ? languageId : navigator}/displayValues/add`,
  displayValueEdit: (languageId, displayValueId = null) => {
    return url`languages/edit/${displayValueId ? languageId : location}/displayValues/edit/${displayValueId || languageId}`
  },
  displayValueDelete: (languageId, displayValueId = null) => {
    return url`languages/edit/${displayValueId ? languageId : location}/displayValues/delete/${displayValueId || languageId}`
  }
}

export default function goto(url, hystoryMethod = 'push') {
  if (typeof url !== 'string') throw new Error('Url must be string type')
  history[hystoryMethod](url)
}
