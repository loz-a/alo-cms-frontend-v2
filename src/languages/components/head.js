import React, { PropTypes } from 'react'
import Head from 'common/components/module/head'
import withTranslator from 'common-app-config/decorators/with-translator'

const LANGUAGES_ROUTE_NAME = 'languages'

const LanguagesHead = ({
  navigation,
  translate
}) => (
  <Head
    header={translate('Site Languages')}
    subheader={translate('List of languages which available on site')}
    icon={Head.getIconByRouteName(navigation, LANGUAGES_ROUTE_NAME)} />
)

LanguagesHead.propTypes = {
  navigation: PropTypes.object.isRequired,
  translate: PropTypes.func.isRequired
}

export default  withTranslator(LanguagesHead)
