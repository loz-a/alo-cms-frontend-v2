import React, { PropTypes } from 'react'
import { Link } from 'react-router'
import { urls } from 'languages/routes/goto'

const ActionsMenu = () => {
  return (
    <div className="ui top attached inverted grey menu borderless">
      <Link
        to={urls.languageNew}
        className="item">
        <i className="large icons">
          <i className="inverted grey world icon"></i>
          <i className="inverted grey corner add icon"></i>
        </i>
        New language
      </Link>
    </div>
  )
}

export default ActionsMenu
