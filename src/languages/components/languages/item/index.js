import React, { PropTypes } from 'react'
import { observable, action, computed } from 'mobx'
import { observer } from 'mobx-react'
import classNames from 'classnames'
import { urls } from 'languages/routes/goto'
import Checkbox from 'common/components/form/input/checkbox'
import Edit from 'common/components/form/link/edit'
import Delete from 'common/components/form/link/delete'
import RowLoading from 'common/components/table/row/loading'
import api from 'languages/api'
import './styles.css'

@observer
class Item extends React.PureComponent {
  @observable isLoading = false

  @computed get language() {
    return this.props.language
  }

  @action handleChange = (evt) => {
    const { toggleEnable } = this.props
    const isEnable = evt.target.checked
    this.isLoading = true

    api
      .toggleEnable(this.language.id, isEnable)
      .then(action(() => {
        this.isLoading = false
        this.language.isEnable = isEnable ? '1' : '0'
      }))
  }

  render() {
    const language = this.language
    const flagCls = `${language.locale.value.split('_')[1].toLowerCase()} flag`
    const rowClas = classNames({'disabled': !language.isEnable})

    if (this.isLoading) return (<RowLoading colSpan={6} />)

    return (
      <tr className={rowClas}>
        <td>
          <div className="ui center aligned">
            <i className={flagCls}></i>
          </div>
        </td>
        <td>{language.language}</td>
        <td>{language.locale.value}</td>
        <td>{language.description}</td>
        <td>
          <Checkbox defaultChecked={language.isEnable === '1' ? true : false} onChange={this.handleChange}/>
        </td>
        <td className="actions">
          <div className="ui horizontal list">
            <Edit href={urls.languageEdit(language.id)} value="Edit"/>
            <Delete href={urls.languageDelete(language.id)} value="Delete"/>
          </div>
        </td>
      </tr>
    )
  }

  static propTypes = {
    language: PropTypes.object.isRequired
  }
}

export default Item
