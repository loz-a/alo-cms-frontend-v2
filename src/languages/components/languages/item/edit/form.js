import React, { PropTypes, cloneElement } from 'react'
import { observable, action, computed, toJS } from 'mobx'
import { observer, inject } from 'mobx-react'

import { LanguagePropType } from 'languages/utils/propTypes'
import goto, { urls } from 'languages/routes/goto'
import api from 'languages/api'
import { checkValidateErrors } from 'common/utils'

import DisplayValues from './display-values'
import Button from 'common/components/form/button/button'
import Submit from 'common/components/form/button/submit'
import Cancel from 'common/components/form/button/cancel'
import Checkbox from 'common/components/form/input/checkbox'

@inject('store')
@observer
class Form extends React.PureComponent {

  @observable language

  constructor(props) {
    super(props)
    this.setLanguage(props.language)
  }

  @action setLanguage = (language) => {
    this.language = { ...language, displayValues: observable.map(language.displayValues) }
  }

  handleChange = (input) => action((evt) => {
    switch(input) {
      case 'isEnable': this.language.isEnable = evt.target.checked
        break
      case 'description': this.language.description = evt.target.value
        break
    }
  })

  handleAddDisplayValue = (evt) => {
    evt.preventDefault()
    goto(urls.displayValueNew(this.language.id))
  }

  handleCancel = (evt) => {
    evt.preventDefault()
    goto(urls.languages())
  }

  handleSubmit = (evt) => {
    evt.preventDefault()

    api
      .saveLanguage(toJS(this.language))
      .then(checkValidateErrors)
      .then((response) => {        
        this.props.store.languages.save(response.data.language)
        goto(urls.languages())
      })

  }

  @computed get isAddDisplayValueBtnAvailable() {
    return (
      this.props.store.locales.size() > this.language.displayValues.size
    )
  }

  render() {
    const { children } = this.props
    const language = this.language

    return (
      <form className="ui form edit-form">
        <div className="three fields">
          <div className="field">
            <label>Language (read only):</label>
            <input type="text" defaultValue={language.language} readOnly />
          </div>
          <div className="field">
            <label>Locale (read only):</label>
            <input type="text" defaultValue={language.locale.value} readOnly />
          </div>
          <div className="field">
            <label>Description:</label>
            <input type="text" defaultValue={language.description} onChange={this.handleChange('description')} />
          </div>
        </div>

        <div className="ui segment">
          <div className="field">
            <Checkbox
              label="Enable/Disable language"
              defaultChecked={this.isEnable}
              onChange={this.handleChange('isEnable')}/>
          </div>
        </div>

        <h4 className="ui dividing header">Language Display Values:</h4>

        <DisplayValues language={language}>
          {children && cloneElement(children, { language })}
        </DisplayValues>

        <div className="actions">
          {this.isAddDisplayValueBtnAvailable
            && <Button onClick={this.handleAddDisplayValue} value="Add Display Value" icon="add" />
          }
          <Submit onClick={this.handleSubmit} value="Save language" />
          <Cancel onClick={this.handleCancel} value="Cancel" />
        </div>
      </form>
    )
  }

  static propTypes = {
    store: PropTypes.object.isRequired,
    language: LanguagePropType.isRequired,
    children: PropTypes.element
  }

}

export default Form
