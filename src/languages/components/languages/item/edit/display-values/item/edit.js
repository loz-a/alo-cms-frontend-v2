import React, { PropTypes } from 'react'
import { action, computed } from 'mobx'
import goto, { urls } from 'languages/routes/goto'
import AddDisplayValue from './add'

class EditDisplayValue extends AddDisplayValue {

  constructor(props) {
    super(props)
    const displayValue = props.language.displayValues.get(this.displayValueIdParam)
    this.populate(displayValue)
  }

  @computed get displayValueIdParam() {
    return this.props.params.displayValueId
  }

  @action populate(data) {
    this.locale = data.locale.value
    this.value = data.value
    this.iconClass = `${data.locale.value.split('_')[1].toLowerCase()} flag`
  }

  @action handleSubmit = (evt) => {
    evt.preventDefault()
    this.props.edit(this.displayValueIdParam, this.value)
    this.isLoading = false

    goto(urls.languageEdit(this.props.params.languageId))
  }

  renderLocale = () => this.locale

  static propTypes = {
    edit: PropTypes.func,
    language: PropTypes.object
  }
}

export default EditDisplayValue
