import React, { PropTypes } from 'react'
import { observable, action } from 'mobx'
import { observer } from 'mobx-react'
import { Link } from 'react-router'
import goto, { urls } from 'languages/routes/goto'
import SelectEnableLocales from './select-enable-locales'
import Submit from 'common/components/form/link/submit'
import Cancel from 'common/components/form/link/cancel'
import RowLoading from 'common/components/table/row/loading'
import Close from 'common/components/form/link/close'

const ROW_COLSPAN = 4

@observer
class AddDisplayValue extends React.Component {
  @observable locale = ''
  @observable value = ''
  @observable iconClass: 'flag icon'

  @observable isLoading = false
  @observable error = ''

  @action handleDisplayValueChange = (evt) => {
    this.value = evt.target.value
  }

  @action handleLocalesChange = (locale, description) => {
    this.locale = locale
    this.iconClass = `${locale.split('_')[1].toLowerCase()} flag`
  }

  @action hadnleCloseError = (evt) => {
    this.locale = ''
    this.value = ''
    this.iconClass = 'flag icon'
    this.isLoading = false
    this.error = ''
  }

  @action handleSubmit = (evt) => {
    evt.preventDefault()
    this.props.add(this.locale, this.value)
    this.isLoading = false

    goto(urls.languageEdit(this.props.params.languageId))
  }

  render() {
    if (this.isLoading) return (<RowLoading colSpan={ROW_COLSPAN}/>)
    if (this.error.length) return this.renderErrors()

    return (
      <tr className="positive">
        <td>
          <div className="ui center aligned">
            <i className={this.iconClass}></i>
          </div>
        </td>

        <td>
          {this.renderLocale()}
        </td>

        <td>
          <div className="ui input">
            <input type="text"
              value={this.value}
              onChange={this.handleDisplayValueChange}
              placeholder="New Display Value"/>
          </div>
        </td>

        <td className="actions">
          <div className="ui horizontal list">
            <Submit href={urls.languageEdit(this.props.params.languageId)} onClick={this.handleSubmit} value="Ok" />
            <Cancel href={urls.languageEdit(this.props.params.languageId)} value="Cancel" />
          </div>
        </td>
      </tr>
    )
  }

  renderErrors = () => (
    <tr className="negative">
      <td colSpan={ROW_COLSPAN}>
        <Close onClick={this.handleCloseError} value="Close"/>
        {this.error}
      </td>
    </tr>
  )

  renderLocale = () => (
    <SelectEnableLocales onChange={this.handleLocalesChange} language={this.props.language} />
  )

  static propTypes = {
    add: PropTypes.func,
    language: PropTypes.object
  }
}

export default AddDisplayValue
