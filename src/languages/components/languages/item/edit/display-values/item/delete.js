import React, { PropTypes } from 'react'
import { computed } from 'mobx'
import { urls } from 'languages/routes/goto'
import Yes from 'common/components/form/link/yes'
import No from 'common/components/form/link/no'

class DeleteDisplayValue extends React.PureComponent {

  @computed get flagCssClass() {
    return `${this.props.displayValue.locale.value.split('_')[1].toLowerCase()} flag`
  }

  onSubmit = (evt) => {
    evt.preventDefault()
    this.props.delete()
  }

  render() {
    const displayValue = this.props.displayValue

    return (
      <tr className="negative">
        <td colSpan="3">
          <span className="ui">
            <i className={this.flagCssClass}></i>
          </span>

          Are you sure you want to delete {displayValue.value} display value?
        </td>

        <td className="actions">
          <div className="ui horizontal list">
            <Yes onClick={this.onSubmit}/>
            <No href={urls.languageEdit(this.props.params.languageId)}/>
          </div>
        </td>

      </tr>
    )
  }

  static propTypes = {
    displayValue: PropTypes.object,
    delete: PropTypes.func
  }

}

export default DeleteDisplayValue
