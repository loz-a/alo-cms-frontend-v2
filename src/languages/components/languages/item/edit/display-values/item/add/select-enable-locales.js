import React, { PropTypes } from 'react'
import { computed } from 'mobx'
import { inject } from 'mobx-react'
import SelectSupportedLocales from 'languages/components/languages/select-supported-locales'

@inject('store')
class SelectEnableLocales extends React.PureComponent {

  @computed get enableLocales() {
    return this.props.store.locales.all()
  }

  filterLocales = (supportedLocales) => {
    if (!Object.keys(supportedLocales).length) return supportedLocales
    const enableLocales = this.enableLocales
    const existsLocales = this.props.language.displayValues.values().map((dv) => dv.locale.value)

    return Object.keys(enableLocales)
      .reduce((acc, id) => {
        const enableLocale = enableLocales[id].value
        if (existsLocales.indexOf(enableLocale) === -1) {
          acc[enableLocale] = supportedLocales[enableLocale]
        }
        return acc
      }, {})
  }

  render() {
    const { store, ...otherProps } = this.props
    return (
      <SelectSupportedLocales {...otherProps } filter={this.filterLocales} />
    )
  }

  static propTypes = {
    store: PropTypes.object.isRequired,
    language: PropTypes.object.isRequired
  }
}

export default SelectEnableLocales
