import React, { PropTypes } from 'react'
import { observable, computed } from 'mobx'
import { observer } from 'mobx-react'
import { Link } from 'react-router'
import { DisplayValuePropType } from 'languages/utils/propTypes'
import { urls } from 'languages/routes/goto'

import Edit from 'common/components/form/link/edit'
import Delete from 'common/components/form/link/delete'
import RowLoading from 'common/components/table/row/loading'

@observer
class Item extends React.PureComponent {
  @observable isLoading = false

  @computed get displayValue() {
    return this.props.displayValue
  }

  @computed get flagCls() {
    return `${this.displayValue.locale.value.split('_')[1].toLowerCase()} flag`
  }

  render() {
    const displayValue = this.displayValue

    if (this.isLoading) return (<RowLoading colSpan={4}/>)

    return (
      <tr>
        <td>
          <div className="ui center aligned">
            <i className={this.flagCls}></i>
          </div>
        </td>
        <td>{displayValue.locale.value}</td>
        <td>{displayValue.value}</td>
        <td className="actions">
          <div className="ui horizontal list">
            <Edit href={urls.displayValueEdit(this.displayValue.id)} value="Edit"/>
            <Delete href={urls.displayValueDelete(this.displayValue.id)} value="Delete"/>
          </div>
        </td>
      </tr>
    )
  }

  static propTypes = {
    displayValue: DisplayValuePropType.isRequired
  }
}


export default Item
