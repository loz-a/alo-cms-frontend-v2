import React, { PropTypes, cloneElement } from 'react'
import { withRouter } from 'react-router'
import { computed, action } from 'mobx'
import { observer, inject } from 'mobx-react'
import shortid from 'shortid'
import { isElementOneOf, ifExpr } from 'common/utils'
import Item from './item'
import AddDisplayValue from './item/add'
import EditDisplayValue from './item/edit'
import DeleteDisplayValue from './item/delete'
import './styles.css'

@withRouter
@inject('store')
@observer
class DisplayValues extends React.PureComponent {

  @computed get language() {
    return this.props.language
  }

  render() {
    return (
      <div className="display-values-container">
        <table className='ui bottom attached celled selectable small table'>
          {this.renderTHead()}
          {this.renderTBody()}
        </table>
      </div>
    )
  }

  renderTHead = () => (
    <thead>
      <tr>
        <th className="one wide center aligned">
          <i className="large flag outline icon"></i>
        </th>
        <th>Locale</th>
        <th>Display Value</th>
        <th>Actions</th>
      </tr>
    </thead>
  )

  renderTBody = () => {
    const { children, params } = this.props
    const { displayValueId: paramDisplayValueId } = params
    const language = this.language

    const ifChildrenIsAdd = ifExpr(isElementOneOf(children, AddDisplayValue)).bind(this)
    const ifChildrenIsDeleteOrEdit = ifExpr(isElementOneOf(children, [DeleteDisplayValue, EditDisplayValue])).bind(this)
debugger
    return (
      <tbody>
        {(language.displayValues.entries().map(([id, displayValue]) => {
          return (paramDisplayValueId === id)
            ? ifChildrenIsDeleteOrEdit(this.renderChildren({
                key: id,
                edit: this.editDisplayValue,
                delete: this.removeDisplayValue(id),
                displayValue
              }))
            : <Item key={displayValue.id} displayValue={displayValue} />
        }))}
        {ifChildrenIsAdd(
          this.renderChildren({ add: this.addDisplayValue, language })
        )}
      </tbody>
    )
  }

  renderChildren = (props = {}) => () => cloneElement(this.props.children, props)

  @action addDisplayValue = (locale, value) => {
    const tempId = shortid.generate()
    this.language.displayValues.set(tempId, {
      id: tempId + '',
      locale: this.props.store.locales.byName(locale),
      value
    })
  }

  @action editDisplayValue = (id, value) => {
    this.language.displayValues.get(id).value = value
  }

  removeDisplayValue = (id) => action(() => {
    this.language.displayValues.delete(id)
  })

  static propTypes = {
    language: PropTypes.object.isRequired,
    store: PropTypes.object.isRequired
  }
}


export default DisplayValues
