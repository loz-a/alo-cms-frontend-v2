import React, { PropTypes } from 'react'
import { computed } from 'mobx'
import { observer, inject } from 'mobx-react'
import Form from './form'
import './styles.css'

@inject('store')
@observer
class Edit extends React.PureComponent {

  @computed get language() {
    const languages = this.props.store.languages.all()
    return languages[this.props.params.languageId]
  }

  render() {    
    return (
      <Form language={this.language}>
        {this.props.children}
      </Form>
    )
  }

  static propTypes = {
    store: PropTypes.object.isRequired
  }
}

export default Edit
