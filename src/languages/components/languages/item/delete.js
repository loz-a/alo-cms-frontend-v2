import React, { PropTypes } from 'react'
import { observable, action, computed } from 'mobx'
import { observer, inject } from 'mobx-react'
import { Link } from 'react-router'
import { LanguagePropType } from 'languages/utils/propTypes'
import goto, { urls } from 'languages/routes/goto'
import api from 'languages/api'
import Yes from 'common/components/form/link/yes'
import No from 'common/components/form/link/no'

@inject('store', 'globalStore')
@observer
class Delete extends React.PureComponent {

  @observable isLoading = false

  @computed get language() {
    return this.props.store.languages.byId(this.props.params.languageId)
  }

  @computed get flagCssClass() {
    return `${this.language.locale.value.split('_')[1].toLowerCase()} flag`
  }

  @action onSubmit = (evt) => {
    evt.preventDefault()
    const { store, globalStore } = this.props
    const language = this.language

    globalStore.enableLoading()
    api
      .deleteLanguage(language.id)
      .then(action(() => {
        store.languages.delete(language)
        globalStore.disableLoading()
        goto(urls.languages())
      }))
  }

  render() {
    const language = this.language

    return (
      <tr className="negative">
        <td colSpan="5">
          <span className="ui">
            <i className={this.flagCssClass}></i>
          </span>

          Are you sure you want to delete language {language.language} and locale {language.locale.value}?
        </td>

        <td className="actions">
          <div className="ui horizontal list">
            <Yes onClick={this.onSubmit}/>
            <No href={urls.languages()}/>
          </div>
        </td>

      </tr>
    )
  }

  static propTypes = {
    store: PropTypes.object.isRequired
  }
}

export default Delete
