import React, { PropTypes } from 'react'
import { observable, action } from 'mobx'
import { observer, inject } from 'mobx-react'
import { Link } from 'react-router'
import Checkbox from 'common/components/form/input/checkbox'
import Submit from 'common/components/form/link/submit'
import Cancel from 'common/components/form/link/cancel'
import RowLoading from 'common/components/table/row/loading'
import SelectSupportedLocales from 'languages/components/languages/select-supported-locales'
import goto, { urls } from 'languages/routes/goto'
import api from 'languages/api'
import { checkValidateErrors } from 'common/utils'
import './styles.css'

const ROW_COLSPAN = 6

@inject('store')
@observer
class Add extends React.Component {
  @observable language = ''
  @observable locale = ''
  @observable description = ''
  @observable isEnable = true
  @observable iconClass = 'flag icon'

  @observable isLoading = false
  @observable error = ''

  handleChange = (input) => action((evt) => {
    switch(input) {
      case 'isEnable': this.isEnable = evt.target.checked
        break
      case 'description': this.description = evt.target.value
        break
    }
  })

  @action handleLocaleChange = (locale, description) => {
    const [language, countryCode] = locale.split('_')
    this.locale = locale
    this.language = language
    this.description = description || this.description
    this.iconClass = `${countryCode.toLowerCase()} flag`
  }

  @action handleCloseError = (evt) => {
    this.language = ''
    this.locale = ''
    this.description = ''
    this.isEnable = true
    this.iconClass = 'flag icon'
    this.error = ''
  }

  @action handleSubmit = (evt) => {
    evt.preventDefault()
    const { store } = this.props
    const data = {
      language: this.language,
      locale: this.locale,
      isEnable: this.isEnable,
      description: this.description
    }

    this.isLoading = true

    api
      .saveLanguage(data)
      .then(checkValidateErrors)
      .then(action((response) => {
          this.isLoading = false
          this.error = ''
          store.languages.save(response.data.language)
          goto(urls.languages())
        }),
        action((error) => {
          this.isLoading = false
          const { response } = error
          if (response && response.status == 200) {
            //@TODO need refactor this ugly code
            this.error = Object.values(Object.values(response.data.validateErrors)[0])[0]
          } else {
            this.error = [response ? response.statusText : 'Something was wrong']
          }
        })
      )
  }

  render() {

    if (this.isLoading) return (<RowLoading colSpan={ROW_COLSPAN}/>)
    if (this.error.length) return this.renderErrors()

    return (
      <tr className="positive">
        <td>
          <div className="ui center aligned">
            <i className={this.iconClass}></i>
          </div>
        </td>
        <td>
          {this.language}
        </td>
        <td>
            <SelectSupportedLocales onChange={this.handleLocaleChange} />
        </td>

        <td>
          <div className="ui input">
            <input type="text"
              value={this.description}
              onChange={this.handleChange('description')}
              placeholder="New Description"/>
          </div>
        </td>

        <td>
          <Checkbox defaultChecked={this.isEnable} onChange={this.handleChange('isEnable')}/>
        </td>

        <td className="actions">
          <div className="ui horizontal list">
            <Submit href={urls.languages()} onClick={this.handleSubmit} value="Ok" />
            <Cancel href={urls.languages()} value="Cancel" />
          </div>
        </td>
      </tr>
    )
  }

  renderErrors = () => (
    <tr className="negative">
      <td colSpan={ROW_COLSPAN}>
        <a className="close-btn" onClick={this.handleCloseError}>
          <i className="cancel icon"></i> Close
        </a>
        {this.error}
      </td>
    </tr>
  )

  static propTypes = {
    store: PropTypes.object.isRequired
  }
}

export default Add
