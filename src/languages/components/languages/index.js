import React, { PropTypes, cloneElement } from 'react'
import { computed } from 'mobx'
import { observer, inject } from 'mobx-react'
import { isElementOneOf, ifExpr } from 'common/utils'
import ActionsMenu from './actions-menu'
import Item from './item'
import Add from './item/add'
import Delete from './item/delete'

@inject('store')
@observer
class Languages extends React.Component {

  @computed get languages() {
    return this.props.store.languages.all()
  }

  render() {
    return (
      <div>
        <ActionsMenu />
        <table className='ui bottom attached celled selectable small table'>
          {this.renderTHead()}
          {this.renderTBody()}
        </table>
      </div>
    )
  }

  renderTBody = () => {
    const languages = this.languages
    const { children, params } = this.props
    const { languageId: paramLanguageId } = params

    const ifChildrenIsAdd = ifExpr(isElementOneOf(children, Add)).bind(this)
    const ifChildrenIsDelete = ifExpr(isElementOneOf(children, Delete)).bind(this)

    return (
      <tbody>
        {Object.values(languages).map((language) => {
          return (paramLanguageId === language.id)
            ? ifChildrenIsDelete(this.renderChildren({ key: language.id }))
            : <Item key={language.id} language={language} />
        })}
        {ifChildrenIsAdd(this.renderChildren())}
      </tbody>
    )
  }

  renderTHead = () => (
    <thead>
      <tr>
        <th className="one wide center aligned">
          <i className="large flag outline icon"></i>
        </th>
        <th>Language</th>
        <th>Locale</th>
        <th>Description</th>
        <th>Enabled</th>
        <th>Actions</th>
      </tr>
    </thead>
  )

  renderChildren = (props = {}) => () => cloneElement(this.props.children, props)

  static propTypes = {
    store: PropTypes.object.isRequired
  }
}

export default Languages
