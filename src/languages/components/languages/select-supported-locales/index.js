import React, { PropTypes } from 'react'
import { computed } from 'mobx'
import { inject } from 'mobx-react'
import jq from 'jquery'
import classNames from 'classnames'
import transition from 'semantic-ui-transition'
import uiDropdown from 'semantic-ui-dropdown'
import Item from './Item'

@inject('store')
class SelectSupportedLocales extends React.Component {

  @computed get supportedLocales () {
    return this.props.store.supportedLocales.all()
  }

  componentDidMount() {
    const { onChange } = this.props

    if (jq.fn.transition === undefined) jq.fn.transition = transition
    jq.fn.dropdown = uiDropdown

    jq(this._dropdown).dropdown({
      onChange: (value, text, $selectedItem) => {
        const selectedItem = $selectedItem[0]
        onChange(selectedItem.getAttribute('data-value'), selectedItem.textContent)
      }
    })
  }

  render() {
    const { filter, onChange, isLoading } = this.props
    const locales = filter ? filter(this.supportedLocales) : this.supportedLocales

    if (!locales) return null

    const className = classNames('ui fluid search selection dropdown', {'loading': isLoading})

    return (
      <div className={className} ref={(c) => this._dropdown = c}>
        <input type="hidden" name="country"/>

        <i className="dropdown icon"></i>
        <div className="default text">Select Country</div>
        <div className="menu">
          {Object.values(locales).map(({name, description}) => (
            <Item key={name} locale={name} description={description}/>
          ))}
        </div>
    </div>
    )
  }

  static propTypes = {
    store: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired,
    filter: PropTypes.func,
    isLoading: PropTypes.bool
  }
}


export default SelectSupportedLocales
