import React, { translate } from 'react'
import ModulesNavigation from 'common/components/modules-navigation'
import Menu from 'common/components/module/menu'
import LanguagesHead from 'languages/components/head'

const App = ({
  children
}) => (
  <ModulesNavigation>
    {(navigationConfig, modulesNavigationVisibilityToggler) => (
      <div>
        <Menu
          navigation={navigationConfig}
          modulesNavigationVisibilityToggler={modulesNavigationVisibilityToggler}/>

        <LanguagesHead navigation={navigationConfig} />

        <div className="ui divider"></div>
        {children}
      </div>
    )}
  </ModulesNavigation>
)

export default App
