import { httpClient } from 'common/utils'
import config from 'languages/config.js'

export default {
  fetchLanguages() {
    return httpClient().get(config.url.languages)
  },

  saveLanguage(data) {
    const normalizedData = { ...data, is_enable: JSON.stringify(+data.isEnable)}
    delete normalizedData.isEnable

    return httpClient().post(config.url.languageSave, normalizedData)
  },

  deleteLanguage(languageId) {
    return httpClient().post(config.url.languageDelete, {languageId})
  },

  toggleEnable(languageId, isEnable) {
    return httpClient().post(config.url.toggleEnable, {languageId, isEnable})
  }

  // fetchSupportedLocales() {
  //   return httpClient().get(config.url.supportedLocales)
  // }
}
