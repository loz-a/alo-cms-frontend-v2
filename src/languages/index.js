import React from 'react'
import { render } from 'react-dom'
import { useStrict } from 'mobx'
import { Provider } from 'mobx-react'
import GlobalStore from 'common/stores/global-store'
import routes from './routes'
import config from './config'
import Store from './store'

import DevTools from 'mobx-react-devtools'

useStrict(true)

const globalStore = new GlobalStore()
globalStore.initToken()
globalStore.initConfig(config.moduleConfigUrl)

const store = new Store(window.INIT_STATE)

if (DEVELOPMENT) {
  window.globalStore = globalStore
  window.store = store
}

render(
  <div className="full-height">
    <Provider globalStore={globalStore} store={store}>
      {routes}
    </Provider>
    <DevTools/>
  </div>,
  document.getElementById('app')
)
