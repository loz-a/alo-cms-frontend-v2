import { PropTypes } from 'react'

export const LocalePropType = PropTypes.shape({
  id: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired
})

export const DisplayValuePropType = PropTypes.shape({
  id: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  locale: LocalePropType.isRequired
})

export const LanguagePropType = PropTypes.shape({
  id: PropTypes.string.isRequired,
  language: PropTypes.string.isRequired,
  locale: LocalePropType.isRequired,
  description: PropTypes.string,
  isEnable: PropTypes.string.isRequired,
  displayValues: PropTypes.objectOf(DisplayValuePropType)
})

export const SupportedLocalePropType = PropTypes.shape({
  'name': PropTypes.string.isRequired,
  'description': PropTypes.string.isRequired
})
