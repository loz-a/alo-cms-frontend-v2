export default {
  moduleConfigUrl: "/api/languages-config.json",
  url: {
    languages: "/api/languages/get",
    languageSave: "/api/languages/save",
    languageDelete: "/api/languages/delete",
    toggleEnable: "/api/languages/toggle-enable"
  },
  routes: {
    languages: {
      index: "/languages",
      add: "add",
      edit: {
        index: "edit/:languageId",
        displayValues: {
          add: "display-values/add",
          edit: "display-values/edit/:displayValueId",
          delete: "display-values/delete/:displayValueId"
        }
      },
      delete: "delete/:languageId"
    }
  }
}
