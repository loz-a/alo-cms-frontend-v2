import React, { PropTypes } from 'react'
import { observer, inject } from 'mobx-react';
import { observable, action } from 'mobx'
import cx from 'classnames'
import withTranslator from 'common-app-config/decorators/with-translator'
import { redirect } from 'common/utils'

@withTranslator
@inject('store')
@observer
class SignIn extends React.Component {

  @observable login
  @observable password
  @observable loading = false
  @observable error = ''

  @action handleSubmit = (evt) => {
    evt.preventDefault()

    const { store } = this.props
    const data = { login: this.login, password: this.password }
    this.loading = true

    store
      .signIn(data)
      .then(
        (response) => {
          redirect(response.redirectRoute)
        },
        action(({ response }) => {
          this.error = response.data.error
          this.loading = false
        })
      )
  }

  @action handleChange = (input) => action((evt) => {
      this[input] = evt.target.value
  })

  render() {
    const { translate } = this.props
    const classNames = cx('ui large form', {loading: this.loading})

    return (
      <div className="ui middle aligned center aligned grid login-form-box">
        <div className="column">

          <form onSubmit={this.handleSubmit} className={classNames}>

            <div className="ui attached message">
              <div className="header">
                  <i className="sign in icon"></i> {translate('Please sign in')}
              </div>
            </div>

            <div className="ui attached fluid segment">

              <div className="field">
                  <div className="ui left icon input">
                      <i className="user icon"></i>
                      <input
                        name="login"
                        placeholder={translate('Login')}
                        autoFocus
                        onChange={this.handleChange('login')}
                        value={this.login}/>
                  </div>
              </div>

              <div className="field">
                  <div className="ui left icon input">
                      <i className="lock icon"></i>
                      <input
                        name="password"
                        type="password"
                        placeholder={translate('Password')}
                        onChange={this.handleChange('password')}/>
                  </div>
              </div>

              <input
                type="submit"
                className="ui fluid large teal submit button"
                defaultValue={translate('Sign in')}/>

            </div>

            {this.error &&
              <div className="ui bottom attached negative message">
                  <i className="warning sign help"></i> {this.error}
              </div>
            }

          </form>
        </div>
      </div>
    )
  }

  static propTypes = {
    store: PropTypes.object.isRequired,
    translate: PropTypes.func.isRequired
  }

}

export default SignIn
