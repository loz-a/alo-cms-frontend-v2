import React, { PropTypes } from 'react'
import { observer, inject } from 'mobx-react';
import { ifExpr } from 'common/utils'
import './styles.css'

@inject('globalStore')
@observer
class App extends React.PureComponent {

  render() {
    const { globalStore, children } = this.props
    const ifLoading = ifExpr(globalStore.loading)

    return (
      <div style={{height: '100%'}} className="ui segment">
        {ifLoading(this.renderLoading, children)}
      </div>
    )
  }

  renderLoading() {
    return (
      <div className="ui active inverted dimmer">
        <div className="ui large text loader"></div>
      </div>
    )
  }

  static propTypes = {
    globalStore: PropTypes.object.isRequired
  }

}

export default App
