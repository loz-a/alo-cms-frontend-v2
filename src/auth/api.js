import { httpClient } from 'common/utils'
import config from 'auth/config'
import api from 'common/api'

api.signIn = (signInData) => httpClient().post(config.api.login, signInData)
api.signOut = () => httpClient().get(config.api.logout)

export default api
