export default {
  moduleConfigUrl: "/auth-config.json",
  api: {
    login: "/api/login",
    logout: "/api/logout"
  }
}
