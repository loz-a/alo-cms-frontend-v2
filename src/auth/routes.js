import { Router, Route, IndexRoute } from 'react-router'
import React from 'react'
import history from 'common/history'

import PageNotFound from 'common/components/page-not-found'
import App from './components/app'
import SignIn from './components/sign-in'
import { loginUri } from 'common/config'

export default (
  <Router history={history}>
    <Route path={loginUri} component={App}>
      <IndexRoute component={SignIn} />
    </Route>
    <Route path="*" component={PageNotFound} />
  </Router>
)
