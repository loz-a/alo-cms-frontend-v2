import globalStore from 'common/stores/global-store'
import api from 'auth/api'
import { jwtData } from 'common/utils'

export default function() {

  this.signIn = (data) => {
    const { token } = api
    return api
      .signIn(data)
      .then(
        (response) => {
          try {
            const { token: responseToken } = response.data
            jwtData(responseToken)
            token.write(responseToken)
          } catch(e) {
            token.clear()
          }
          return response.data
        },
        (error) => {
          token.clear()
          throw error
        }
      )
  }

}
