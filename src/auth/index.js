import React from 'react'
import { render } from 'react-dom'
import { useStrict } from 'mobx'
import { Provider } from 'mobx-react'
import routes from 'auth/routes'
import config from 'auth/config'
import GlobalStore from 'common/stores/global-store'
import Store from 'auth/stores'

useStrict(true)

const globalStore = new GlobalStore()
globalStore.initConfig(config.moduleConfigUrl, false)

if (DEVELOPMENT) {
  window.globalStore = globalStore
}

const store = new Store()

render(
  <Provider globalStore={globalStore} store={store}>
    {routes}
  </Provider>,
  document.getElementById('app')
)
