import React, { PropTypes } from 'react'
import Head from 'common/components/module/head'
import withTranslator from 'common-app-config/decorators/with-translator'

const ADMIN_ROUTE_NAME = 'admin'

const AdminHead = ({
  navigation,
  translate
}) => (
  <Head
    header={translate('Admin dashboard')}
    subheader={translate('Dashboard with general statistical information')}
    icon={Head.getIconByRouteName(navigation, ADMIN_ROUTE_NAME)} />
)

AdminHead.propTypes = {
  navigation: PropTypes.object.isRequired,
  translate: PropTypes.func.isRequired
}

export default withTranslator(AdminHead)
