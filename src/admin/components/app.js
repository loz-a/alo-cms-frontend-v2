import React, { translate } from 'react'
import ModulesNavigation from 'common/components/modules-navigation'
import Menu from 'common/components/module/menu'
import AdminHead from 'admin/components/head'

const App = ({
  children
}) => (
  <ModulesNavigation>
    {(navigationConfig, modulesNavigationVisibilityToggler) => (
      <div>
        <Menu
          navigation={navigationConfig}
          modulesNavigationVisibilityToggler={modulesNavigationVisibilityToggler}/>

        <AdminHead navigation={navigationConfig} />

        <div className="ui divider"></div>
        {children}
      </div>
    )}
  </ModulesNavigation>
)

export default App
