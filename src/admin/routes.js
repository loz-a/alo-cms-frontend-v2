import React from 'react'
import { Router, Route, IndexRoute } from 'react-router'
import history from 'common/history'
import PageNotFound from 'common/components/page-not-found'
import App from 'admin/components/app'
import Dashboard from 'admin/components/dashboard'
import config from 'admin/config'

export default (
  <Router history={history}>
    <Route path={config.routes.admin} component={App}>
      <IndexRoute component={Dashboard} />
    </Route>
    <Route path="*" component={PageNotFound} />
  </Router>
)
