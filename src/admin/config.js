export default {
  moduleConfigUrl: '/admin-config.json',
  routes: {
    admin: '/admin'
  }
}
