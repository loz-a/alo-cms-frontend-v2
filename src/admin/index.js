import React from 'react'
import { render } from 'react-dom'
import { useStrict } from 'mobx'
import { Provider } from 'mobx-react'
import GlobalStore from 'common/stores/global-store'
import routes from './routes'
import config from './config'

useStrict(true)

const globalStore = new GlobalStore()
globalStore.initToken()
globalStore.initConfig(config.moduleConfigUrl)

if (DEVELOPMENT) {
  window.globalStore = globalStore
}

render(
  <Provider globalStore={globalStore}>
    {routes}
  </Provider>,
  document.getElementById('app')
)
